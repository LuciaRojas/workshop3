<?php
include_once 'ClConexion/ClConexion.php';
$sentencia_select=$con->prepare('SELECT * FROM clientes ORDER BY id_cliente ASC');
$sentencia_select->execute();
$resultado=$sentencia_select->fetchAll();



?>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="../css/style.css">
    <script src="js/funciones.js"></script>
    <title>Empleados</title>
</head>
<body>
    <header>
        <div class="hide" id="contenedor_menu">
        </div>
        <nav id="contenedor_menu2" >  
         </nav> 
    </header>

<div class="contenedor">
    <h2>Empleados</h2>
  
    <a href="pagina/insercli.html" class="btn_nuevo">Nuevo</a>
        <table border="1">
            <tr class="head">
            <td class="id_ocultar">Id</td>
            <td>Nombre</td>
            <td>Apellido</td>
            <td>Numero de Telefono</td>
            <td>Email</td>
            <td colspan="2">Acción</td>
            </tr>
            <tr>
            <?php foreach($resultado as $fila) :?>
                <td class="id_ocultar"> <?php echo $fila['id_cliente']; ?></td>

                <td> <?php echo $fila['nombre']; ?></td>
                <td> <?php echo $fila['apellido']; ?></td>
                <td> <?php echo $fila['num_telefono']; ?></td>
                <td> <?php echo $fila['email']; ?></td>
                
                <td> <a href="pagina/updcli.php?id_cliente=<?php echo $fila['id_cliente']; ?>" class="btn__update">Editar</a></td>      
                <td> <a href="pagina/delcli.php?id_cliente=<?php echo $fila['id_cliente']; ?>" class="btn__delete">Eliminar</a></td> 
            </tr>
            <?php endforeach ?>
        </table>

</div>
    
</body>
</html>|