<?php
include_once '../ClConexion/ClConexion.php';

    if(isset($_GET['id_cliente'])){
        $id_cliente=(int) $_GET['id_cliente'];
        $buscar_id=$con->prepare('SELECT * FROM clientes WHERE id_cliente=:id_cliente LIMIT 1');
		$buscar_id->execute(array(
			':id_cliente'=>$id_cliente
		));
		$resultado=$buscar_id->fetch();
	}else{
        echo "<script> alert('error nq');</script>";
	}


    
	if(isset($_POST['guardar'])){
        $nombre=$_POST['nombre'];
        $apellido=$_POST['apellido'];
        $num_telefono=$_POST['num_telefono'];
        $email=$_POST['email'];
        $id_cliente=(int) $_GET['id_cliente'];

        if(!empty($nombre) && !empty($apellido) && !empty($num_telefono) && !empty($email)){
       
                
                $consulta_update=$con->prepare(' UPDATE clientes SET  
					nombre=:nombre,
                    apellido=:apellido,
					num_telefono=:num_telefono,
					email=:email
					WHERE id_cliente=:id_cliente;'
				);
				$consulta_update->execute(array(
                    ':nombre' =>$nombre,
                    ':apellido' =>$apellido,
					':num_telefono' =>$num_telefono,
                    ':email' =>$email,
					':id_cliente' =>$id_cliente
				));
                echo "<script> alert('Se han guardado los cambios');</script>";
                header('Location: ../index.php');
		}else{
			echo "<script> alert('Los campos estan vacios');</script>";
		}
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex,nofollow">

    <link rel="stylesheet" href="../css/style.css">
    <title>Editar Clientes</title>
</head>
<body>



    <div class="contenedor">
    <h2>Editar Datos</h2>
    <form action="updcli.php" method="post">
    <div class="form-group">
            <input type="text" name="nombre" placeholder="Nombre"  value="<?php if($resultado) echo $resultado['nombre']; ?>" class="input__text" required="" pattern="[a-zA-Z]+" >
            <input type="text" name="apellido" placeholder="Apellido"  value="<?php if($resultado) echo $resultado['apellido']; ?>" class="input__text"  required="" pattern="[a-zA-Z]+" >
        </div>


        <div class="form-group">
            <input type="text" name="num_telefono" placeholder="número teléfono"  value="<?php if($resultado) echo $resultado['num_telefono']; ?>" class="input__text" required="" pattern="[0-9]+">
            <input type="text" name="email" placeholder="email" value="<?php if($resultado) echo $resultado['email']; ?>" class="input__text" required="">
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
         
          <input type="submit" name="guardar" value="Guardar" class="btn__primary">
          
        </div>
    </form>
    
    </div>
    
</body>
</html>
